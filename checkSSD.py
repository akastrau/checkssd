#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  4 16:21:53 2017
@description: check SSD health with smartctltool
@author: Adrian Kastrau

"""
import os
import subprocess
import sys
    
print("\t\tSSD health tool for Linux")
print("\t\tAdrian Kastrau, v. 1.0, 2017")
print('')
if sys.version_info < (3,5):
    print ('\tPython 3 is required to run this script!')
    exit(1)
if sys.platform.lower() != "linux" and sys.platform.lower() != "linux2":
    print("\tThis program is intended to use in Linux-like systems")
    exit(1)
if os.geteuid() != 0:
    print("\tYou have to be a root!")
    exit(1)
else:
    try:
        output = subprocess.run(['smartctl', '-a', '/dev/sda'], stdout=subprocess.PIPE, check=True).stdout
    except FileNotFoundError:
        print("\tYou have to install smartctl tool! In Ubuntu-like system install smartmontools package.\n")
        exit(1)
    except Exception as ex:
        print("\tSomething went wrong!", ex)
        exit(1)
    out = bytes.decode(output)
    out = out.splitlines()
    data = []
    for line in out:
        if "ID#" in line or "Media_Wearout_Indicator" in line:
            columns = line.split()
            data.append(columns)
    print("\tSSD health: ", int(data[1][3]), "\b%")